В датасете представлены композиции из стримингового сервиса Spotify. Каждый трек описывается своими музыкальными свойствами (тональность, модальность, громкость и т.д.).

Количество записей: 2017.

Выбранные признаки:
duration_ms (количественная): длительность трека в миллисекундах;
key (категориальная): тональность трека;
mode (бинарная): модальность трека (1 для мажорной тональности, 0 для минорной);
time_signature (категориальная): тактовый размер трека;
energy (количественная): “энергичность” трека (от 0 до 1);
valence (количественная): “позитивность” трека (от 0 до 1);
instrumentalness (количественная): инструментальность трека (от 0 до 1);
tempo (количественная): средний темп трека (beats per minute);
loudness (количественная): громкость трека в децибелах;

Источник: https://www.kaggle.com/geomack/spotifyclassification

Примеры задач: нахождение корреляции между темпом трека и его “энергичностью”; проверка гипотезы, что инструментальные композиции в среднем длиннее остальных; классификация треков по жанрам.


